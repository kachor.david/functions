const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string' || isNaN(str1) || isNaN(str2)) {
    return false
  }
  let biggerStrLength = str1.length >= str2.length ? str1.length : str2.length
  let finalString = ''
  for (let i = 0; i < biggerStrLength; i++) {
    finalString += ((!str1[i] ? 0 : +str1[i]) + (!str2[i] ? 0 : +str2[i]))
  }
  return finalString
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let numberOfPosts = listOfPosts.filter(e => e.author === authorName).length
  let numberOfComments = 0
  listOfPosts.forEach(e => {
    if (e.comments) {
      numberOfComments += e.comments.filter(el => el.author === authorName).length
    }
  })
  return `Post:${numberOfPosts},comments:${numberOfComments}`
};

const tickets = (people) => {
  let twentyFiveBillAmount = 0, fiftyBillAmount = 0
  return people.map(el => {
    if (el == 25) {
      twentyFiveBillAmount++
      return 'YES'
    } else if (el == 50) {
      if (twentyFiveBillAmount === 0) {
        return 'NO'
      } else {
        twentyFiveBillAmount--
        fiftyBillAmount++
        return 'YES'
      }
    } else if (el == 100) {
      if ((twentyFiveBillAmount === 0 || fiftyBillAmount === 0) && twentyFiveBillAmount < 3) {
        return 'NO'
      } else if (twentyFiveBillAmount > 0 && fiftyBillAmount > 0) {
        twentyFiveBillAmount--
        fiftyBillAmount--
        return 'YES'
      } else if (twentyFiveBillAmount >= 3) {
        twentyFiveBillAmount -= 3
        return 'YES'
      }
    }
  }).filter(el => el === 'YES').length === people.length ? 'YES' : 'NO'
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
